package com.camila.laberinto8005;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.view.View;

/**
 * Created by Usuario on 18/05/2016.
 */
public class Bola extends View {


    String pararTodos = "";

    String partesLab[];

    int uno = 0, dos = 0, tres = 0, cuatro = 0, cinco = 0, seis = 0, siete = 0,
            ocho = 0, nueve = 0, dies = 0, once = 0, doce = 0, trece = 0, catorce = 0,
            quince = 0, diesies = 0;


    float x;
    float y;
    private RectF rectangulo = new RectF();


    public Bola(Context context, float x, float y, float anchoPantalla, float altoPantalla) {
        super(context);
        this.x = x;
        this.y = y;
        rectangulo.set(0, 0, anchoPantalla / Laberinto.POSICIONES_ITEM[0].length, altoPantalla / Laberinto.POSICIONES_ITEM.length);

    }


    @Override
    public float getX() {
        return x;
    }

    @Override
    public float getY() {
        return y;
    }

    public float getAnchoCelda() {
        return rectangulo.width();
    }

    public float getAltoCelda() {
        return rectangulo.height();
    }

    private Float[] getOrigen() {
        Float[] origen = new Float[2];
        for (Item i : Laberinto.getItems()) {
            if (i.getTipo() == 7) {
                origen[0] = i.getX();
                origen[1] = i.getY();
                break;
            }
        }
        return origen;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bola);

        int ancho = (int) getAnchoCelda();
        int alto = (int) getAltoCelda();

        mBitmap = Bitmap.createScaledBitmap(mBitmap, ancho, alto, false);


        canvas.drawBitmap(mBitmap, x, y, null);
    }


    public String pararTodos() {
        String par = "";

        for (Item i : Laberinto.getItems()) {

            if (i.getTipo() == 2) {
                if (i.getX() - getAnchoCelda() / 1.7 < x && i.getX() + getAnchoCelda() / 1.7 > x
                        && i.getY() - getAltoCelda() / 1.7 < y && i.getY() + getAltoCelda() / 1.7 > y
                        && i.getTipo() != 0 && i.getTipo() != 7) {
                    par = par + "-" + i.getTipo();
                }

            } else if (i.getTipo() == 3) {
                if (i.getX() - getAnchoCelda() / 1.7 < x && i.getX() + getAnchoCelda() / 1.7 > x
                        && i.getY() - getAltoCelda() / 1.7 < y && i.getY() + getAltoCelda() / 1.7 > y
                        && i.getTipo() != 0 && i.getTipo() != 7) {
                    par = par + "-" + i.getTipo();
                }

            } else {
                if (i.getX() - getAnchoCelda() < x && i.getX() + getAnchoCelda() > x
                        && i.getY() - getAltoCelda() < y && i.getY() + getAltoCelda() > y
                        && i.getTipo() != 0 && i.getTipo() != 7) {
                    par = par + "-" + i.getTipo();
                }
            }


        }

        return par;
    }

    public int move0(float x, float y, int inicio) {

        int retorno = 0;
        pararTodos = pararTodos();

        uno = 0;
        dos = 0;
        tres = 0;
        cuatro = 0;
        cinco = 0;
        seis = 0;
        siete = 0;
        ocho = 0;
        nueve = 0;
        dies = 0;
        once = 0;
        doce = 0;
        trece = 0;
        catorce = 0;
        quince = 0;
        diesies = 0;

        partesLab = pararTodos.split("-");
        for (int i = 0; i < partesLab.length; i++) {
            if (partesLab[i].contains("1")) {
                if (partesLab[i].length() > 1) {
                } else {
                    uno = 1;
                }
            }
            if (partesLab[i].contains("2")) {
                if (partesLab[i].length() > 1) {
                } else {
                    dos = 1;
                }
            }
            if (partesLab[i].contains("3")) {
                if (partesLab[i].length() > 1) {
                } else {
                    tres = 1;
                }
            }
            if (partesLab[i].contains("4")) {
                if (partesLab[i].length() > 1) {
                } else {
                    cuatro = 1;
                }
            }
            if (partesLab[i].contains("5")) {
                if (partesLab[i].length() > 1) {
                } else {
                    cinco = 1;
                }
            }
            if (partesLab[i].contains("6")) {
                if (partesLab[i].length() > 1) {
                } else {
                    seis = 1;
                }
            }
            if (partesLab[i].contains("7")) {
                siete = 1;
            }
            if (partesLab[i].contains("8")) {
                ocho = 1;
            }
            if (partesLab[i].contains("9")) {
                nueve = 1;
            }
            if (partesLab[i].contains("10")) {
                dies = 1;
            }
            if (partesLab[i].contains("11")) {
                once = 1;
            }
            if (partesLab[i].contains("12")) {
                doce = 1;
            }
            if (partesLab[i].contains("13")) {
                trece = 1;
            }
            if (partesLab[i].contains("14")) {
                catorce = 1;
            }
            if (partesLab[i].contains("15")) {
                quince = 1;
            }
            if (partesLab[i].contains("16")) {
                diesies = 1;
            }
        }

        //Log.e("PARAR ", "" + pararTodos);
        //Log.e("PARAR@@ ", "UNO: " + uno + " DOS: " + dos + " TRES: " + tres + " CUATRO: " + cuatro + " CINCO: " + cinco + " SEIS: " + seis
        //        + " SIETE: " + siete + " OCHO " + ocho + " NUEVE " + nueve + " DIEZ " + dies + " ONCE " + once + " DOCE " + doce);


        //INICIO DEL JUEGO
        if (inicio == 1) {
            iniciar(x, y);
            //Log.e("MOVER", "INICIO JUEGO" + pararTodos);
        }
        //MOVER EN  2
        else if (dos == 1) {
            try {
                Thread.sleep(200);
                iniciar(x, y);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        //GANO EL JUEGO
        else if (tres == 1) {
            retorno = 1;
        }
        //MOVER EN 1
        else if (uno == 1 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 0 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 0 && catorce == 0 && quince == 0 && diesies == 0) {
            move1(x, y);
            //Log.e("MOVER", "1: " + pararTodos);
        }
        //MOVER EN 4
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 1 && cinco == 0 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 0 && catorce == 0 && quince == 0 && diesies == 0) {
            move4(x, y);
        }
        //MOVER EN 11 SIN 4 CON 1
        else if (uno == 1 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 0 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 1 && doce == 0 && trece == 0 && quince == 0 && diesies == 0) {
            move11(x, y);
        }
        //MOVER EN 11 CON 4
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 1 && cinco == 0 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 1 && doce == 0 && trece == 0 && catorce == 0 && quince == 0 && diesies == 0) {
            move4(x, y);
        }
        //MOVER EN 6
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 0 && seis == 1 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 0 && catorce == 0 && quince == 0 && diesies == 0) {
            move6(x, y);
            //Log.e("MOVER", "6: " + pararTodos);
        }
        //MOVER EN 6 CON 4
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 1 && cinco == 0 && seis == 1 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 0 && catorce == 0 && quince == 0 && diesies == 0) {
            move6(x, y);
            //Log.e("MOVER", "6: " + pararTodos);
        }
        //MOVER EN 10 SIN 6 CON 4
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 1 && cinco == 0 && seis == 1 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 1 && once == 0 && doce == 0 && trece == 0 && catorce == 0 && quince == 0 && diesies == 0) {
            move10(x, y);

        }
        //MOVER EN 5
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 1 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 0 && catorce == 0 && quince == 0 && diesies == 0) {
            move5(x, y);
            //Log.e("MOVER", "6: " + pararTodos);
        }
        //MOVER EN 12 SIN 5 CON 1
        else if (uno == 1 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 0 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 1 && trece == 0 && catorce == 0 && quince == 0 && diesies == 0) {
            move12(x, y);
        }
        //MOVER EN 12 CON 5 SIN 0
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 1 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 1 && trece == 0 && catorce == 0 && quince == 0 && diesies == 0) {
            move5(x, y);
        }
        //MOVER EN 13
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 0 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 1 && catorce == 0 && quince == 0 && diesies == 0) {
            moverLibre(x, y);
        }
        //MOVER EN 13 CON 4 CON 1
        else if (uno == 1 && dos == 0 && tres == 0 && cuatro == 1 && cinco == 0 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 1 && catorce == 0 && quince == 0 && diesies == 0) {
            move1(x, y);
        }
        //MOVER EN 13 CON 4 CON 6
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 1 && cinco == 0 && seis == 1 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 1 && catorce == 0 && quince == 0 && diesies == 0) {
            move6(x, y);
        }
        //MOVER EN 13 CON 4 CON 14
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 1 && cinco == 0 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 1 && catorce == 1 && quince == 0 && diesies == 0) {
            moveEsquinaPrimeroDerecha(x, y);
        }
        //MOVER EN 14
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 0 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 0 && catorce == 1 && quince == 0 && diesies == 0) {
            moverLibre(x, y);
        }
        //MOVER EN 14 CON 13
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 0 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 1 && catorce == 1 && quince == 0 && diesies == 0) {
            moverLibre(x, y);
        }
        //MOVER EN 1 CON 13 CON 5
        else if (uno == 1 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 1 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 1 && catorce == 0 && quince == 0 && diesies == 0) {
            move1(x, y);
        }
        //MOVER EN 13 CON 5 CON 14
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 1 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 1 && catorce == 1 && quince == 0 && diesies == 0) {
            moveEsquinaMedioIzquierda(x, y);
        }
        //MOVER EN 14 CON 5
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 1 && cinco == 0 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 0 && catorce == 1 && quince == 0 && diesies == 0) {
            move5(x, y);
        }
        //MOVER EN 15
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 0 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 0 && catorce == 0 && quince == 1 && diesies == 0) {
            moverLibre(x, y);
        }
        //MOVER EN 15 CON 5
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 1 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 0 && catorce == 0 && quince == 1 && diesies == 0) {
            move6(x, y);
        }
        //MOVER EN 15 CON 5 CON 14
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 1 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 0 && catorce == 1 && quince == 1 && diesies == 0) {
            moveEsquinaMedioDerecha(x, y);
        }
        //MOVER EN 15 CON 5 CON 5
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 1 && seis == 1 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 0 && catorce == 0 && quince == 1 && diesies == 0) {
            move6(x, y);
        }
        //MOVER EN 15 CON 14
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 0 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 0 && catorce == 1 && quince == 1 && diesies == 0) {
            moverLibre(x, y);
        }
        //MOVER EN 15 CON 13
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 0 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 1 && catorce == 0 && quince == 1 && diesies == 0) {
            moverLibre(x, y);
        }
        //MOVER EN 15 CON 13 CON 4
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 1 && cinco == 0 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 1 && catorce == 0 && quince == 1 && diesies == 0) {
            moveEsquinaPrimeroIzquierda(x, y);
        }
        //MOVER EN 16 CON 5
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 1 && seis == 0 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 0 && catorce == 0 && quince == 0 && diesies == 1) {
            move5(x, y);
            //Log.e("MOVER", "6: " + pararTodos);
        }
        //MOVER EN 16 CON 6
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 0 && seis == 1 && siete == 0 && ocho == 0
                && nueve == 0 && dies == 0 && once == 0 && doce == 0 && trece == 0 && catorce == 0 && quince == 0 && diesies == 1) {
            move16(x, y);
        }
        //MOVER EN CERO
        else if (uno == 0 && dos == 0 && tres == 0 && cuatro == 0 && cinco == 0 && seis == 0 && siete == 0
                && dies == 0 && once == 0 && doce == 0 && trece == 0 && catorce == 0 && quince == 0 && diesies == 0) {
            this.x += x;
            this.y += y;
            JuegoActivity.xBueno = this.x;
            JuegoActivity.yBueno = this.y;
        }

        this.invalidate();

        return retorno;
    }

    public void move1(float x, float y) {
        this.y += y;
        JuegoActivity.yBueno = this.y;
        if (x > 0) {
            this.x = JuegoActivity.xBueno;
            JuegoActivity.xBueno = this.x;
        } else {
            this.x = JuegoActivity.xBueno + x;
            JuegoActivity.xBueno = this.x;
        }
    }


    public void move3(float x, float y) {

    }

    public void move4(float x, float y) {
        this.x += x;
        JuegoActivity.xBueno = this.x;
        if (y > 0) {
            this.y = JuegoActivity.yBueno;
            JuegoActivity.yBueno = this.y;
        } else {
            this.y = JuegoActivity.yBueno + y;
            JuegoActivity.yBueno = this.y;
        }
    }

    public void move5(float x, float y) {
        this.x += x;
        JuegoActivity.xBueno = this.x;
        if (y < 0) {
            this.y = JuegoActivity.yBueno;
        } else {
            this.y = JuegoActivity.yBueno + y;
            JuegoActivity.yBueno = this.y;
        }
    }

    public void move6(float x, float y) {
        this.y += y;
        JuegoActivity.yBueno = this.y;
        if (x < 0) {
            this.x = JuegoActivity.xBueno;
            JuegoActivity.xBueno = this.x;
        } else {
            JuegoActivity.xBueno = this.x;
            this.x = JuegoActivity.xBueno + x;
        }
    }

    public void move10(float x, float y) {
        float a = JuegoActivity.yBueno + y;
        if (a < JuegoActivity.yBueno) {
            this.y += y;
            //this.y += y * rebote;
            JuegoActivity.yBueno = this.y;
        }
        float s = JuegoActivity.xBueno + x;
        if (s > JuegoActivity.xBueno) {
            this.x += x;
            //this.x += x * rebote;
            JuegoActivity.xBueno = this.x;
        }
    }

    public void move11(float x, float y) {
        float a = JuegoActivity.yBueno + y;
        if (a < JuegoActivity.yBueno) {
            this.y += y;
            JuegoActivity.yBueno = this.y;
        }
        float s = JuegoActivity.xBueno + x;
        if (s < JuegoActivity.xBueno) {
            this.x += x;
            JuegoActivity.xBueno = this.x;
        }
    }

    public void move12(float x, float y) {
        float a = JuegoActivity.yBueno + y;
        if (a > JuegoActivity.yBueno) {
            this.y += y;
            JuegoActivity.yBueno = this.y;
        }
        float s = JuegoActivity.xBueno + x;
        if (s < JuegoActivity.xBueno) {
            this.x += x;
            JuegoActivity.xBueno = this.x;
        }
    }

    public void move16(float x, float y) {
        float a = JuegoActivity.yBueno + y;
        if (a > JuegoActivity.yBueno) {
            this.y += y;
            JuegoActivity.yBueno = this.y;
        }
        float s = JuegoActivity.xBueno + x;
        if (s > JuegoActivity.xBueno) {
            this.x += x;
            JuegoActivity.xBueno = this.x;
        }
    }

    public void iniciar(float x, float y) {
        this.x = getOrigen()[0];
        this.y = getOrigen()[1];
        JuegoActivity.xBueno = this.x;
        JuegoActivity.yBueno = this.y;
    }

    public void moverLibre(float x, float y) {
        this.x += x;
        this.y += y;
        JuegoActivity.xBueno = this.x;
        JuegoActivity.yBueno = this.y;
    }


    public void moveEsquinaPrimeroIzquierda(float x, float y) {
        //11 PURO
        float a = JuegoActivity.yBueno + y;
        if (a < JuegoActivity.yBueno) {
            this.y += y;
            JuegoActivity.yBueno = this.y;
        }
        float s = JuegoActivity.xBueno + x;
        if (s < JuegoActivity.xBueno) {
            this.x += x;
            JuegoActivity.xBueno = this.x;
        }
    }

    public void moveEsquinaPrimeroDerecha(float x, float y) {
        //10 PURO
        float a = JuegoActivity.yBueno + y;
        if (a < JuegoActivity.yBueno) {
            this.y += y;
            JuegoActivity.yBueno = this.y;
        }
        float s = JuegoActivity.xBueno + x;
        if (s > JuegoActivity.xBueno) {
            this.x += x;
            JuegoActivity.xBueno = this.x;
        }
    }

    public void moveEsquinaMedioIzquierda(float x, float y) {
        //11 INVERTIDO EN Y
        float a = JuegoActivity.yBueno + y;
        if (a > JuegoActivity.yBueno) {
            this.y += y;
            JuegoActivity.yBueno = this.y;
        }
        float s = JuegoActivity.xBueno + x;
        if (s < JuegoActivity.xBueno) {
            this.x += x;
            JuegoActivity.xBueno = this.x;
        }
    }

    public void moveEsquinaMedioDerecha(float x, float y) {
        //11 INVERTIDO
        float a = JuegoActivity.yBueno + y;
        if (a > JuegoActivity.yBueno) {
            this.y += y;
            JuegoActivity.yBueno = this.y;
        }
        float s = JuegoActivity.xBueno + x;
        if (s > JuegoActivity.xBueno) {
            this.x += x;
            JuegoActivity.xBueno = this.x;
        }
    }

    public void setPosition(float newX, float newY) {
        this.x = newX;
        this.y = newY;
        this.invalidate();
    }
}