package com.camila.laberinto8005;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class FinalActivity extends AppCompatActivity {

    List<Partida> partidas;

    String tiempoJugado;
    long tiempoJugadoGuardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_final);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        partidas = new ArrayList<>();

        TextView tvMsj = (TextView) findViewById(R.id.tv_msj);
        tiempoJugado = getIntent().getExtras().getString("tiempoVer");
        tiempoJugadoGuardar = getIntent().getExtras().getLong("tiempoGuardar");

        if (Partida.find(Partida.class, "tiempo = ?", tiempoJugadoGuardar + "").size() == 0) {
            Partida partida = new Partida(tiempoJugadoGuardar, tiempoJugado);
            Partida.save(partida);
        }

        if (Partida.find(Partida.class, "tiempo < ?", tiempoJugadoGuardar + "").size() > 0) {
            tvMsj.setText("Time: " + tiempoJugado);
        } else {
            tvMsj.setText("New Record\n Time: " + tiempoJugado);

        }

    }

    @Override
    public void onBackPressed() {
        /*super.onBackPressed();*/
        Intent back = new Intent(FinalActivity.this, InicioActivity.class);
        startActivity(back);
        finish();
    }
}
