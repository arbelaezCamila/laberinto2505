package com.camila.laberinto8005;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

public class InicioActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_inicio);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public void onCLick(View v) {
        Intent go = new Intent();
        switch (v.getId()) {
            case R.id.btn_play:
                go = new Intent(InicioActivity.this, JuegoActivity.class);
                startActivity(go);
                InicioActivity.this.finish();
                break;
            case R.id.btn_score:
                //go = new Intent(InicioActivity.this, JuegoActivity.class);
                break;

        }

    }
}
