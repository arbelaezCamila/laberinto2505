package com.camila.laberinto8005;

/**
 * Created by Usuario on 25/05/2016.
 */
public class Item {

    float x;
    float y;
    int tipo;


    public Item() {
    }

    public Item(float x, float y, int tipo) {
        this.x = x;
        this.y = y;
        this.tipo = tipo;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
}
