package com.camila.laberinto8005;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class JuegoActivity extends AppCompatActivity {

    private SensorManager sensorManager;
    private Sensor accelerometer;
    Display display;
    FrameLayout screenJuego;
    Laberinto mLaberinto;
    Bola mBola;
    MiEventListener mListener;
    float sensorX;
    float sensorY;
    public static float xBueno = 0;
    public static float yBueno = 0;
    public static int iniciar = 0;
    TimerTask tareaTiempo;
    Timer timerJuego;
    String tiempoJugado = "";

    long milisegundos = 0;

    TextView txtTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        txtTime = (TextView) findViewById(R.id.tv_tiempo);


        Bitmap[] mBitmaps = new Bitmap[]{
                BitmapFactory.decodeResource(getResources(), R.drawable.none),
                BitmapFactory.decodeResource(getResources(), R.drawable.muro),
                BitmapFactory.decodeResource(getResources(), R.drawable.hoyo),
                BitmapFactory.decodeResource(getResources(), R.drawable.meta),
                BitmapFactory.decodeResource(getResources(), R.drawable.muro),
                BitmapFactory.decodeResource(getResources(), R.drawable.muro),
                BitmapFactory.decodeResource(getResources(), R.drawable.muro),
                BitmapFactory.decodeResource(getResources(), R.drawable.inicio),
                BitmapFactory.decodeResource(getResources(), R.drawable.none),
                BitmapFactory.decodeResource(getResources(), R.drawable.none),
                BitmapFactory.decodeResource(getResources(), R.drawable.muro),
                BitmapFactory.decodeResource(getResources(), R.drawable.muro),
                BitmapFactory.decodeResource(getResources(), R.drawable.muro),
                BitmapFactory.decodeResource(getResources(), R.drawable.none),
                BitmapFactory.decodeResource(getResources(), R.drawable.none),
                BitmapFactory.decodeResource(getResources(), R.drawable.none),
                BitmapFactory.decodeResource(getResources(), R.drawable.muro),

        };

        WindowManager mWindowManager = (WindowManager) this.getSystemService(this.WINDOW_SERVICE);
        display = mWindowManager.getDefaultDisplay();

        screenJuego = (FrameLayout) findViewById(R.id.screen_juego);

        mLaberinto = new Laberinto(mBitmaps, getAnchoAltoDisplay(display)[0], getAnchoAltoDisplay(display)[1]);
        VistaLaberinto vLaberinto = new VistaLaberinto(JuegoActivity.this, mLaberinto);
        mBola = new Bola(JuegoActivity.this, 0, 0, getAnchoAltoDisplay(display)[0], getAnchoAltoDisplay(display)[1]);

        vLaberinto.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mBola.setPosition(event.getX(), event.getY());
                return true;
            }

        });
        screenJuego.addView(vLaberinto);
        screenJuego.addView(mBola);

        sensorManager = (SensorManager) JuegoActivity.this.getSystemService(SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mListener = new MiEventListener();
        sensorManager.registerListener(mListener, accelerometer, SensorManager.SENSOR_DELAY_GAME);
        JuegoActivity.iniciar = 0;
        obtenerDuracion();
    }


    class MiEventListener implements SensorEventListener {

        @Override
        public void onSensorChanged(SensorEvent event) {


            switch (JuegoActivity.iniciar) {
                case 0:
                    for (Item i : Laberinto.getItems()) {
                        if (i.getTipo() == 7) {
                            mBola.move0(i.getX(), i.getY() / 2, 1);

                            JuegoActivity.iniciar = 2;
                            break;
                        }

                    }
                    break;
                case 2:
                    sensorX = event.values[1];
                    sensorY = event.values[0];
                    int retorno = mBola.move0(sensorX, sensorY, 0);
                    if (retorno == 1) {
                        ganar();
                    }
                    break;

            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    }


    public static float[] getAnchoAltoDisplay(Display d) {

        Point point = new Point();
        d.getSize(point);
        float anchoPantalla = point.x;
        float altoPantalla = point.y;
        float[] wh = new float[]{anchoPantalla, altoPantalla};

        return wh;
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(mListener);
        sensorX = 0;
        sensorY = 0;

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(mListener, accelerometer, SensorManager.SENSOR_DELAY_GAME);

    }

    public void ganar() {
        sensorManager.unregisterListener(mListener);
        if (tiempoJugado != "") {
            //  Toast.makeText(JuegoActivity.this, "Tiempo: " + tiempoJugado, Toast.LENGTH_SHORT).show();
            Intent go = new Intent(JuegoActivity.this, FinalActivity.class);
            go.putExtra("tiempoVer", tiempoJugado);
            go.putExtra("tiempoGuardar", milisegundos);
            startActivity(go);
            JuegoActivity.this.finish();
        } else {
            Toast.makeText(JuegoActivity.this, "Error con el tiempo", Toast.LENGTH_SHORT).show();
        }

        if (tareaTiempo != null) {
            tareaTiempo.cancel();
        }
        if (timerJuego != null) {
            timerJuego.cancel();
        }
    }

    public void obtenerDuracion() {
        milisegundos = 0;
        if (tareaTiempo != null) {
            tareaTiempo.cancel();
        }
        tareaTiempo = new TimerTask() {
            android.os.Handler handler = new android.os.Handler();

            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        milisegundos = milisegundos + 1000;

                        int minutos = (int) ((milisegundos / 1000) / 60);
                        int segundos = (int) ((milisegundos / 1000) % 60);
                        String min = "", seg = "";
                        if (minutos <= 9) {
                            min = "0" + minutos + ":";
                        } else {
                            min = "" + minutos + ":";
                        }
                        if (segundos <= 9) {
                            seg = "0" + segundos + "";
                        } else {
                            seg = "" + segundos + "";
                        }
                        tiempoJugado = "" + min + "" + seg;
                        txtTime.setText("Tiempo: " + tiempoJugado);
                        //Toast.makeText(JuegoActivity.this, ""+min+""+seg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        timerJuego = new Timer();
        timerJuego.scheduleAtFixedRate(tareaTiempo, 0, 1000);

    }//cierre del metodo
}
