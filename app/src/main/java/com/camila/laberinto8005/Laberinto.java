package com.camila.laberinto8005;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.RectF;

import java.util.ArrayList;

/**
 * Created by Usuario on 18/05/2016.
 */
public class Laberinto {

    public static int STATUS_DRAW = 0;

    private RectF rectangulo = new RectF();
    private Bitmap[] bitmaps;
    private int posItem[][];



    private float anchoPantalla, altoPantalla;

    static ArrayList<Item> items = new ArrayList<>();

    //1 -> derecha
    //6 -> izquierda
    //5 -> arriba
    //4 -> abajo


    public static final int POSICIONES_ITEM[][] = {
            {16, 16, 05, 05, 05, 05, 05, 05, 05, 05, 05, 05, 05, 05, 05, 05, 05, 05, 05, 05, 05, 12, 12},
            {06, 02, 00, 00, 00, 00, 00, 00, 00, 02, 01, 01, 06, 02, 00, 00, 00, 00, 00, 00, 00, 02, 01},
            {06, 00, 00, 00, 00, 00, 00, 00, 00, 00, 01, 01, 06, 00, 00, 00, 00, 00, 00, 00, 00, 00, 01},
            {06, 00, 00, 15, 00, 00, 00, 14, 00, 00, 01, 01, 06, 00, 00, 15, 00, 00, 00, 14, 00, 00, 01},
            {06, 00, 00, 13, 04, 04, 04, 13, 00, 00, 01, 01, 06, 00, 00, 13, 04, 04, 04, 13, 00, 00, 01},
            {06, 00, 00, 00, 01, 01, 06, 00, 00, 00, 01, 01, 06, 00, 00, 00, 01, 01, 06, 00, 00, 00, 01},
            {06, 00, 00, 00, 01, 01, 06, 00, 00, 00, 01, 01, 06, 00, 00, 00, 01, 01, 06, 00, 00, 00, 01},
            {06, 00, 00, 00, 01, 01, 06, 00, 00, 00, 01, 01, 06, 00, 02, 00, 01, 01, 06, 00, 00, 00, 01},
            {06, 00, 00, 00, 01, 01, 06, 00, 00, 00, 01, 01, 06, 00, 00, 00, 01, 01, 06, 00, 00, 00, 01},
            {06, 00, 00, 00, 01, 01, 06, 00, 00, 00, 01, 01, 06, 00, 00, 00, 01, 01, 06, 02, 00, 00, 01},
            {06, 00, 00, 00, 01, 01, 06, 00, 00, 13, 05, 05, 05, 15, 00, 00, 01, 01, 06, 00, 00, 00, 01},
            {06, 00, 00, 00, 01, 01, 06, 00, 00, 14, 00, 00, 00, 14, 00, 00, 01, 01, 06, 00, 00, 00, 01},
            {06, 00, 00, 00, 01, 01, 06, 00, 00, 00, 00, 00, 00, 00, 00, 00, 01, 01, 06, 00, 00, 00, 01},
            {06, 00, 03, 00, 01, 01, 06, 02, 00, 00, 00, 00, 00, 00, 00, 02, 01, 01, 06, 00, 07, 00, 01},
            {10, 04, 04, 11, 11, 04, 10, 04, 04, 04, 04, 04, 04, 04, 04, 11, 11, 04, 10, 04, 04, 11, 11},

    };

    public Laberinto(Bitmap[] bitmaps, float anchoPantalla, float altoPantalla) {
        this.bitmaps = bitmaps;
        this.posItem = POSICIONES_ITEM;
        this.anchoPantalla = anchoPantalla;
        this.altoPantalla = altoPantalla;

        float xCeldas = POSICIONES_ITEM[0].length;
        float yCeldas = POSICIONES_ITEM.length;
        rectangulo.set(0, 0, anchoPantalla / xCeldas, altoPantalla / yCeldas);

    }


    public float getAnchoCelda() {
        return rectangulo.width();
    }

    public float getAltoCelda() {
        return rectangulo.height();
    }

    public void dibujarLaberinto(Canvas canvas) {

        int piezaX = 0;
        int piezaY = 0;

        float coorX = -0;
        float coorY = -0;


        while (piezaY < posItem.length && coorY <= altoPantalla) {
            piezaX = 0;
            coorX = -0;

            while (piezaX < posItem[piezaY].length && coorX <= anchoPantalla) {

                if (bitmaps[posItem[piezaY][piezaX]] != null) {

                    if (coorX + getAnchoCelda() >= 0 && coorY + getAltoCelda() >= 0) {

                        rectangulo.offsetTo(coorX, coorY);
                        canvas.drawBitmap(bitmaps[posItem[piezaY][piezaX]], null, rectangulo, null);

                        if (STATUS_DRAW == 0) {
                            if (items.size() <= (posItem[0].length * posItem.length)) {

                                Item item = new Item(coorX, coorY, posItem[piezaY][piezaX]);
                                items.add(item);

                            } else {
                                STATUS_DRAW++;
                            }
                        }

                    }
                }


                piezaX++;
                coorX += getAnchoCelda();
            }

            piezaY++;
            coorY += getAltoCelda();

        }

    }

    public static ArrayList<Item> getItems() {
        return items;
    }
}

