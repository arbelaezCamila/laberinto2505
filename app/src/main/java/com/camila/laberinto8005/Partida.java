package com.camila.laberinto8005;

import com.orm.SugarRecord;

/**
 * Created by Usuario on 13/06/2016.
 */

public class Partida extends SugarRecord {

    long tiempo;
    String tiempover;

    public Partida(long tiempo, String tiempover) {
        this.tiempo = tiempo;
        this.tiempover = tiempover;
    }

    public String getTiempover() {
        return tiempover;
    }

    public void setTiempover(String tiempover) {
        this.tiempover = tiempover;
    }

    public Partida() {
    }


    public long getTiempo() {
        return tiempo;
    }

    public void setTiempo(long tiempo) {
        this.tiempo = tiempo;
    }
}
