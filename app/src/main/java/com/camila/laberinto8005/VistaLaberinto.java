package com.camila.laberinto8005;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;

public class VistaLaberinto extends View {


    Laberinto laberinto;

    public VistaLaberinto(Context context, Laberinto lab) {
        super(context);
        this.laberinto = lab;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        laberinto.dibujarLaberinto(canvas);
        invalidate();
    }
}